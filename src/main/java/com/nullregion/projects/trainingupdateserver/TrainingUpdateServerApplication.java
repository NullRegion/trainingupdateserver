package com.nullregion.projects.trainingupdateserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingUpdateServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingUpdateServerApplication.class, args);
	}

}
